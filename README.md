# SDC Reference Implementation Model

SDC Reference Implementation Model is a set of JAXB classes to marshall/unmarshall the data models of 
- Devices Profile for Web Services and referenced standards
- IEEE 11073-10207: Point-of-care medical device communication Part 10207: Domain Information and Service Model for Service-Oriented Point-of-Care Medical Device Communication

## License

SDC Reference Implementation Model is licensed under the MIT license, see [LICENSE](LICENSE) file.
