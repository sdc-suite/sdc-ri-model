import com.github.spotbugs.snom.Confidence

plugins {
    id("buildlogic.java-conventions")
    id("buildlogic.deploy")
    alias(libs.plugins.com.github.spotbugs.plugin)
}

dependencies {
    api(libs.org.glassfish.jaxb.jaxb.xjc)
    api(libs.org.jvnet.jaxb.jaxb.plugins.tools)
    spotbugs(libs.com.github.spotbugs)
}

description = "BICEPS model plugins"

tasks.withType<Checkstyle>().configureEach {
    // code modified from external source violates checkstyle rules
    enabled = false
}

spotbugs {
    reportLevel = Confidence.MEDIUM
}