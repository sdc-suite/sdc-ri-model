package org.somda.sdc.biceps.model.plugins;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.XMLConstants;

public class PluginUtil {

    /**
     * The code is based on the <b>isEqualNode</b> methods of the classes
     * com.sun.org.apache.xerces.internal.dom.NodeImpl,
     * com.sun.org.apache.xerces.internal.dom.ParentNode
     * and com.sun.org.apache.xerces.internal.dom.ElementImpl, but it will not consider the namespace prefix when
     * comparing two nodes.
     * <p>
     * Tests whether two nodes are equal.
     * <br>This method tests for equality of nodes, not sameness
     * (i.e., whether the two nodes are references to the same object) which can be tested with <code>Node.isSameNode()</code>.
     * All nodes that are the same will also be equal, though the reverse may not be true.
     * <br>Two nodes are equal if and only if the following conditions are satisfied:
     * <ul>
     *     <li>The two nodes are of the same type.</li>
     *     <li>The following string attributes are equal: <code>localName</code>,
     *     <code>namespaceURI</code>, <code>nodeValue</code>. This is: they are both
     *     <code>null</code>, or they have the same length and are character for character identical.</li>
     *     <li>The <code>attributes</code> <code>NamedNodeMaps</code> are equal. This is: they are both
     *     <code>null</code>, or they have the same length and for each node that exists in one map there is a node
     *     that exists in the other map with the same <code>nodeValue</code> and <code>namespaceURI</code>, although
     *     not necessarily at the same index.</li>
     *     <li>The <code>childNodes</code> <code>NodeLists</code> are equal. This is: they are both <code>null</code>,
     *     or they have the same length and contain equal nodes at the same index. Note that normalization can affect
     *     equality; to avoid this, nodes should be normalized before being compared.</li>
     * </ul>
     * <br>On the other hand, the following do not affect equality: the <code>ownerDocument</code>,
     * <code>baseURI</code>, and <code>parentNode</code> attributes, the <code>specified</code> attribute for
     * <code>Attr</code> nodes, the <code>schemaTypeInfo</code> attribute for <code>Attr</code> and
     * <code>Element</code> nodes, the <code>Text.isElementContentWhitespace</code> attribute for <code>Text</code>
     * nodes, as well as any user data or event listeners registered on the nodes.
     * <p>
     * <b>Note:</b>  As a general rule, anything not mentioned in the
     * description above is not significant in consideration of equality
     * checking.
     * <p>
     *
     * @param left  node to compare equality with.
     * @param right node to compare equality with.
     * @return Returns <code>true</code> if the nodes are equal,
     * <code>false</code> otherwise.
     */
    public static boolean customIsEqualNode(Node left, Node right) {
        if (left == right) {
            return true;
        }
        if (left.getNodeType() != right.getNodeType()) {
            return false;
        }
        if (left.getNodeName() == null) {
            if (right.getNodeName() != null) {
                return false;
            }
        }
        if (left.getLocalName() == null) {
            if (right.getLocalName() != null) {
                return false;
            }
        } else {
            if (!left.getLocalName().equals(right.getLocalName())) {
                return false;
            }
        }
        if (left.getNamespaceURI() == null) {
            if (right.getNamespaceURI() != null) {
                return false;
            }
        } else {
            if (!left.getNamespaceURI().equals(right.getNamespaceURI())) {
                return false;
            }
        }
        if (left.getNodeValue() == null) {
            if (right.getNodeValue() != null) {
                return false;
            }
        } else {
            if (!left.getNodeValue().equals(right.getNodeValue())) {
                return false;
            }
        }
        boolean hasAttrs = left.hasAttributes();
        if (hasAttrs != right.hasAttributes()) {
            return false;
        }
        if (hasAttrs) {
            NamedNodeMap map1 = left.getAttributes();
            NamedNodeMap map2 = right.getAttributes();

            if (getLengthWithoutNamespaceDeclaration(map1) != getLengthWithoutNamespaceDeclaration(map2)) {
                return false;
            }

            int len = map1.getLength();
            for (int i = 0; (i < len); i += 1) {
                Node n1 = map1.item(i);
                if (isAttributeANamespaceDeclaration(n1)) {
                    continue;
                }
                boolean equalAttributeFound = false;
                for (int j = 0; (j < len); j += 1) {
                    Node n2 = map2.item(j);
                    if (isAttributeANamespaceDeclaration(n2)) {
                        continue;
                    }
                    if (customIsEqualNode(n1, n2)) {
                        equalAttributeFound = true;
                        break;
                    }
                }
                if (!equalAttributeFound) {
                    return false;
                }
            }
        }
        Node child1 = left.getFirstChild();
        Node child2 = right.getFirstChild();
        while ((child1 != null) && (child2 != null)) {
            if (!customIsEqualNode(child1, child2)) {
                return false;
            }
            child1 = child1.getNextSibling();
            child2 = child2.getNextSibling();
        }
        if (child1 != child2) {
            return false;
        }
        return true;
    }

    private static int getLengthWithoutNamespaceDeclaration(NamedNodeMap map) {
        int len = 0;
        for (int i = 0; (i < map.getLength()); i += 1) {
            if (!isAttributeANamespaceDeclaration(map.item(i))) {
                len += 1;
            }
        }
        return len;
    }

    private static boolean isAttributeANamespaceDeclaration(Node node) {
        if (node.getLocalName() != null && node.getLocalName().equals(XMLConstants.XMLNS_ATTRIBUTE) && node.getPrefix() == null) {
            return true;
        }
        if (node.getNamespaceURI() != null && node.getNamespaceURI().equals(XMLConstants.XMLNS_ATTRIBUTE_NS_URI)) {
            return true;
        }
        if (node.getPrefix() != null && node.getPrefix().equals(XMLConstants.XMLNS_ATTRIBUTE)) {
            return true;
        }
        return false;
    }
}
