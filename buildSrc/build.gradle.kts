plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
}

gradlePlugin {
    plugins {
        create("XjcPlugin") {
            id = "org.somda.sdc.xjc"
            implementationClass = "org.somda.sdc.XjcPlugin"
        }
    }
}

