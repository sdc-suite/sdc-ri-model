plugins {
    `java-library`
    checkstyle
}

repositories {
    mavenLocal()
    mavenCentral()
}

group = "org.somda.sdc"
version = "6.1.0-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
    withJavadocJar()
    withSourcesJar()
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

tasks.withType<Javadoc> {
    options.encoding = "UTF-8"
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

checkstyle {
    toolVersion = "10.15.0"
    configFile = rootProject.file("checkstyle.xml")
}

tasks.register("releaseCheck") {
    group = "publishing"
    description = "Checks if the project only depends on non-SNAPSHOT versions."
    project.configurations.forEach { configuration ->
        configuration.allDependencies.forEach {
            logger.debug("releaseCheck (${configuration.name}): Checking ${project.name}: ${it.name} in ${it.version}")
            check(it.version?.contains("-SNAPSHOT") == false) {
                "Dependencies with SNAPSHOT versions are not allowed." +
                    " Violating: ${it.name}, found in configuration ${configuration.name}"
            }
        }
    }
}