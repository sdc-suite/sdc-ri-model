# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [6.0.0] - 2024-10-02

### Added
- Add Nullable annotation to setters and getters which support null values. (#5)

### Changed

- Use JAXB 4.0 from jakarta instead of 2.x from javax. (#2)
- Update jaxb-plugins to 4.0.6 to avoid transitive dependency javaparser 1.x (outdated, LGPL-3.0+). (#3)
- Bump libraries to get rid of some flawed transitive dependencies. (#6)
- Move to gradle as build system. (#7)
- Require at least Java 17. (#7)
- Reduce IDE warnings, use some Java 17 features. (#12)

## [5.0.0] - 2023-09-05

### Added

- Existing modules from sdc-ri `biceps-model`, `dpws-model`, as well as a `common-model`-module, which contains relevant parts of sdc-ri `common`-module and
a new `biceps-model-plugin`-module. (#1)