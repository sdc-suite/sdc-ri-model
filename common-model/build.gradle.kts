import com.github.spotbugs.snom.Confidence

plugins {
    id("buildlogic.java-conventions")
    id("buildlogic.deploy")
    alias(libs.plugins.com.github.spotbugs.plugin)
}

dependencies {
    api(libs.org.checkerframework.checker.qual)
    api(libs.com.google.inject.guice)
    api(libs.com.google.inject.guice.assistedinject)
    api(libs.org.junit.jupiter.junit.jupiter.api)
    api(libs.org.glassfish.jaxb.jaxb.core)
    api(libs.org.glassfish.jaxb.jaxb.runtime)
    api(libs.jakarta.xml.bind.jakarta.xml.bind.api)
    api(libs.org.jetbrains.annotations)
    spotbugs(libs.com.github.spotbugs)

    testImplementation(libs.org.junit.jupiter.junit.jupiter.api)
    testImplementation(libs.org.junit.jupiter.junit.jupiter.engine)
}

description = "SDCri is a set of Java libraries that implements a network communication framework conforming with" +
    " the IEEE 11073 SDC specifications. This project implements common functionality used for SDCri."

tasks.named<Checkstyle>("checkstyleTest") {
    // at least one test intentionally violates rules to test other code
    enabled = false
}

spotbugs {
    reportLevel = Confidence.MEDIUM
}