package org.somda.sdc.common.model;

import org.jetbrains.annotations.Nullable;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

import java.math.BigInteger;
import java.time.Instant;

/**
 * Adapter class to convert Participant Model timestamps to Java instants and vice versa.
 */
public class TimestampAdapter extends XmlAdapter<BigInteger, Instant> {
    @Override
    public Instant unmarshal(@Nullable BigInteger v) {
        return v == null ? null : Instant.ofEpochMilli(v.longValue());
    }

    @Override
    public BigInteger marshal(@Nullable Instant v) {
        return v == null ? null : BigInteger.valueOf(v.toEpochMilli());
    }
}
