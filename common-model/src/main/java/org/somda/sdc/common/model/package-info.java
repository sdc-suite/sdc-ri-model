/**
 * Common utilities.
 */
@DefaultQualifier(value = NotNull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.common.model;

import org.jetbrains.annotations.NotNull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
