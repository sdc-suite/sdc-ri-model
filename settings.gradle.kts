rootProject.name = "sdc-ri-model"

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include(":common-model")
include(":biceps-model-plugins")
include(":dpws-model")
include(":biceps-model")

