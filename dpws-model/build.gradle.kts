plugins {
    id("buildlogic.java-conventions")
    id("buildlogic.deploy")
    id("org.somda.sdc.xjc")
}

val jaxb: Configuration by configurations.creating

dependencies {
    api(projects.commonModel)
    api(libs.org.glassfish.jaxb.jaxb.core)
    api(libs.org.glassfish.jaxb.jaxb.runtime)
    api(libs.jakarta.xml.bind.jakarta.xml.bind.api)
    api(libs.io.github.threeten.jaxb.threeten.jaxb.core)
    api(libs.org.jvnet.jaxb.jaxb.plugins)
    implementation(libs.jakarta.xml.bind.jakarta.xml.bind.api)
}

description = "Implements the model for Devices Profile for Web-Services (DPWS) 1.1 and referenced standards."

val schemaDir = "src/main/resources"

xjc {
    schemaLocation = layout.projectDirectory.dir(schemaDir)
    args = listOf(
        "-nv", // <strict>false</strict>
    )
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<Javadoc> {
    // generated code causes warnings, can't do anything about it :(
    (options as CoreJavadocOptions).addBooleanOption("Xdoclint:none", true)
}

tasks.withType<Checkstyle>().configureEach {
    // generated code
    enabled = false
}