package org.somda.sdc.biceps.model.participant;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.jvnet.jaxb.lang.CopyTo;

import java.util.List;

class CopyableTest {

    static Unmarshaller unmarshaller;

    @BeforeAll
    static void initJaxb() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance("org.somda.sdc.biceps.model.extension:" + "org.somda.sdc.biceps.model.message");
        unmarshaller = context.createUnmarshaller();
    }

    @Test
    @DisplayName("Copy a coded value and its translation, ensure identity of new object differs")
    void copyCodedValue() {
        final var codedValue = new CodedValue();
        codedValue.setCode("code");
        codedValue.setCodingSystem("system");

        final var translation = new CodedValue.Translation();
        translation.setCode("translation code");
        translation.setCodingSystem("private system");
        codedValue.setTranslation(List.of(translation));

        final var copyTarget = processCopy(codedValue);

        Assertions.assertEquals("code", copyTarget.getCode());
        Assertions.assertEquals("system", copyTarget.getCodingSystem());

        Assertions.assertEquals(System.identityHashCode(codedValue), System.identityHashCode(codedValue));
        Assertions.assertNotEquals(System.identityHashCode(codedValue), System.identityHashCode(copyTarget));

        Assertions.assertEquals(1, copyTarget.getTranslation().size());
        Assertions.assertEquals(translation.getCode(), copyTarget.getTranslation().get(0).getCode());
        Assertions.assertEquals(translation.getCodingSystem(), copyTarget.getTranslation().get(0).getCodingSystem());
        Assertions.assertNotEquals(System.identityHashCode(translation), System.identityHashCode(copyTarget.getTranslation().get(0)));
    }

    public <T extends CopyTo> T processCopy(T input) {
        return copyToExplicit(input);
    }

    private <T extends CopyTo> T copyToExplicit(T data) {
        //noinspection unchecked
        return (T) data.copyTo(null);
    }
}
