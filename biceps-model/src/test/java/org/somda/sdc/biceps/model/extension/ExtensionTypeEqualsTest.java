package org.somda.sdc.biceps.model.extension;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.somda.sdc.biceps.model.message.Retrievability;
import org.somda.sdc.biceps.model.message.RetrievabilityInfo;
import org.somda.sdc.biceps.model.message.RetrievabilityMethod;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.UnmarshalException;
import jakarta.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;

class ExtensionTypeEqualsTest {

    final String unknownExtension = """
        <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
            <what:ItIsNotKnown xmlns:what="123.456.789"><what:Unknown>What does this mean?</what:Unknown></what:ItIsNotKnown>
            </ext:Extension>""";
    static Unmarshaller unmarshaller;

    @BeforeAll
    static void initJaxb() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance("org.somda.sdc.biceps.model.extension:" + "org.somda.sdc.biceps.model.message");
        unmarshaller = context.createUnmarshaller();
    }

    @Test
    void notEqualsNull() {
        final ExtensionType extension = new ExtensionType();
        final ExtensionType extension2 = null;
        Assertions.assertNotEquals(extension, extension2);
    }

    @Test
    void notEqualsDifferentClass() {
        final ExtensionType extension = new ExtensionType();
        final String extension2 = "ExtensionType";
        Assertions.assertNotEquals(extension, extension2);
    }

    @Test
    void equalsSameObject() {
        final ExtensionType extension = new ExtensionType();

        final Retrievability retrievability = buildRetrievability(
            buildRetrievabilityInfo(RetrievabilityMethod.GET, Duration.ofSeconds(5)),
            buildRetrievabilityInfo(RetrievabilityMethod.STRM, Duration.ofMinutes(2))
        );

        extension.setAny(List.of(retrievability));
        final ExtensionType extension2 = extension;
        Assertions.assertEquals(extension, extension2);
    }

    @Test
    void notEqualsRightAnyEmpty() {
        final ExtensionType left = new ExtensionType();

        final Retrievability retrievability = buildRetrievability(
            buildRetrievabilityInfo(RetrievabilityMethod.GET, Duration.ofSeconds(5)),
            buildRetrievabilityInfo(RetrievabilityMethod.STRM, Duration.ofMinutes(2))
        );

        left.setAny(List.of(retrievability));

        Assertions.assertNotEquals(left, new ExtensionType());
    }

    @Test
    void notEqualsLeftAnyEmpty() {
        final ExtensionType right = new ExtensionType();

        final Retrievability retrievability = buildRetrievability(
            buildRetrievabilityInfo(RetrievabilityMethod.EP, Duration.ofSeconds(5)),
            buildRetrievabilityInfo(RetrievabilityMethod.STRM, Duration.ofSeconds(5))
        );

        right.setAny(List.of(retrievability));

        Assertions.assertNotEquals(new ExtensionType(), right);
    }

    @Test
    void notEqualsAnyNotSameSize() {
        final ExtensionType left = new ExtensionType();
        final ExtensionType right = new ExtensionType();

        final Retrievability retrievability = buildRetrievability(
            buildRetrievabilityInfo(RetrievabilityMethod.PER, Duration.ZERO),
            buildRetrievabilityInfo(RetrievabilityMethod.STRM, Duration.ofSeconds(5))
        );

        final Retrievability retrievability2 = buildRetrievability(
            buildRetrievabilityInfo(RetrievabilityMethod.PER, Duration.ZERO)
        );

        left.setAny(List.of(retrievability));
        right.setAny(List.of(retrievability, retrievability2));

        Assertions.assertNotEquals(left, right);
    }

    @Test
    void equalsAnyWithKnownExtensions() {
        final ExtensionType left = new ExtensionType();
        final ExtensionType right = new ExtensionType();

        final Retrievability retrievability = buildRetrievability(
            buildRetrievabilityInfo(RetrievabilityMethod.PER, Duration.ZERO)
        );

        final Retrievability retrievability2 = buildRetrievability(
            buildRetrievabilityInfo(RetrievabilityMethod.PER, Duration.ZERO)
        );

        left.setAny(List.of(retrievability));
        right.setAny(List.of(retrievability2));

        Assertions.assertEquals(left, right);
    }

    @Test
    void equalsAnyWithUnknownExtensions() throws JAXBException {
        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsAnyWithUnknownExtensionsAndDifferentPrefix() throws JAXBException {

        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789"><what:Unknown>What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <who:ItIsNotKnown xmlns:who="123.456.789"><who:Unknown>What does this mean?</who:Unknown></who:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsAnyWithUnknownExtensionsWithCDATAContent() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789">
                <![CDATA[<some test data & stuff>]]>
                <what:Unknown>What does this mean?<![CDATA[Test this CDATA section]]></what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <who:ItIsNotKnown xmlns:who="123.456.789">
                <![CDATA[<some test data & stuff>]]>
                <who:Unknown>What does this mean?<![CDATA[Test this CDATA section]]></who:Unknown></who:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void notEqualsDifferentCDATAContent() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789">
                <what:Unknown>What does this mean?<![CDATA[This CDATA section]]></what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <who:ItIsNotKnown xmlns:who="123.456.789">
                <![CDATA[<some test data & stuff>]]>
                <who:Unknown>What does this mean?<![CDATA[That CDATA section]]></who:Unknown></who:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertNotEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsSameAttributesDifferentPrefix() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789" testAttribute="This is a test"><what:Unknown>What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <who:ItIsNotKnown xmlns:who="123.456.789" testAttribute="This is a test"><who:Unknown>What does this mean?</who:Unknown></who:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsSameAttributes() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789" testAttribute="Some Attribute"><what:Unknown amazing="I love XML">What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789" testAttribute="Some Attribute"><what:Unknown amazing="I love XML">What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsSameAttributesDifferentOrder() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789" testAttribute="Some Attribute"><what:Unknown amazing="I love XML" proto="Nett hier, aber haben Sie schon von Protobuf gehört?">What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789" testAttribute="Some Attribute"><what:Unknown proto="Nett hier, aber haben Sie schon von Protobuf gehört?" amazing="I love XML">What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void notEqualsDifferentAttributes() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789" proto="Nett hier, aber haben Sie schon von Protobuf gehört?"><what:Unknown>What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789" proto="XML macht Spaß!"><what:Unknown>What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertNotEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsAnyWithMixedNamespaceDeclarations() throws JAXBException {

        String unknownExtensionDifferentNamespaceDeclaration = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension" xmlns:what="123.456.789">
                <what:ItIsNotKnown><what:Unknown>What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtensionDifferentNamespaceDeclaration))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsAnyWithMixedExtensions() throws JAXBException {
        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension))).getValue();

        final Retrievability retrievability = buildRetrievability(
            buildRetrievabilityInfo(RetrievabilityMethod.PER, Duration.ZERO)
        );

        extensionType1.any.add(retrievability);
        extensionType2.any.add(retrievability);

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void compareUnknownWithNestedKnownExtension() throws JAXBException {

        // check if retrievability is known
        var retrievability = unmarshaller.unmarshal(new StringReader("<msg:Retrievability xmlns:msg=\"http://standards.ieee.org/downloads/11073/11073-10207-2017/message\"/>"));
        Assertions.assertInstanceOf(Retrievability.class, retrievability);

        String unknownWithNestedKnownExtension = """
               <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension" xmlns:msg="http://standards.ieee.org/downloads/11073/11073-10207-2017/message" xmlns:what="123.456.789">
                    <what:ItIsNotKnown><msg:Retrievability/></what:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownWithNestedKnownExtension))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownWithNestedKnownExtension))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsExtensionWithDefaultNamespace() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<ItIsNotKnown xmlns="123.456.789" proto="Nett hier, aber haben Sie schon von Protobuf gehört?"><Unknown>What does this mean?</Unknown></ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<ItIsNotKnown xmlns="123.456.789" proto="Nett hier, aber haben Sie schon von Protobuf gehört?"><Unknown>What does this mean?</Unknown></ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void notEqualsExtensionWithDifferentDefaultNamespace() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<ItIsNotKnown xmlns="123.456.789" proto="Nett hier, aber haben Sie schon von Protobuf gehört?"><Unknown>What does this mean?</Unknown></ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<ItIsNotKnown xmlns="123.456.788" proto="Nett hier, aber haben Sie schon von Protobuf gehört?"><Unknown>What does this mean?</Unknown></ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertNotEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsDifferentFormattingSelfClosingTag() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<ItIsNotKnown xmlns="123.456.789" proto="Nett hier, aber haben Sie schon von Protobuf gehört?"><Unknown/></ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<ItIsNotKnown xmlns="123.456.789" proto="Nett hier, aber haben Sie schon von Protobuf gehört?"><Unknown></Unknown></ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void equalsIgnoreNamespaceDeclarationWhenComparingTheAttributeLength() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<who:ItIsNotKnown xmlns:who="123.456.789" testAttribute="This is a test"><who:Unknown>What does this mean?</who:Unknown></who:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension" xmlns:pm="http://standards.ieee.org/downloads/11073/11073-10207-2017/participant">
                "<who:ItIsNotKnown xmlns:who="123.456.789" testAttribute="This is a test"><who:Unknown>What does this mean?</who:Unknown></who:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertEquals(extensionType1, extensionType2);
    }

    @Test
    void notEqualsDontIgnoreNonNamespaceDeclarationWhenComparingTheAttributeLength() throws JAXBException {
        String unknownExtension1 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<who:ItIsNotKnown xmlns:who="123.456.789" testAttribute="This is a test"><who:Unknown>What does this mean?</who:Unknown></who:ItIsNotKnown>
                </ext:Extension>""";
        String unknownExtension2 = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                "<who:ItIsNotKnown xmlns:who="123.456.789" testAttribute="This is a test" otherAttribute="shouldNotBeIgnored"><who:Unknown>What does this mean?</who:Unknown></who:ItIsNotKnown>
                </ext:Extension>""";

        ExtensionType extensionType1 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension1))).getValue();
        ExtensionType extensionType2 = ((JAXBElement<ExtensionType>) unmarshaller.unmarshal(new StringReader(unknownExtension2))).getValue();

        Assertions.assertNotEquals(extensionType1, extensionType2);
    }

    @Test
    void throwsOnDuplicateDeclarationOfAttributes() {
        String unknownExtensionWithDuplicateAttribute = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <ext:Extension xmlns:ext="http://standards.ieee.org/downloads/11073/11073-10207-2017/extension">
                <what:ItIsNotKnown xmlns:what="123.456.789" xmlns:what="123.456.789"><what:Unknown>What does this mean?</what:Unknown></what:ItIsNotKnown>
                </ext:Extension>""";

        Assertions.assertThrows(UnmarshalException.class, () -> unmarshaller.unmarshal(new StringReader(unknownExtensionWithDuplicateAttribute)));
    }

    private RetrievabilityInfo buildRetrievabilityInfo(final RetrievabilityMethod retrievabilityMethod, final Duration duration) {
        final RetrievabilityInfo retrievabilityInfo = new RetrievabilityInfo();
        retrievabilityInfo.setMethod(retrievabilityMethod);
        retrievabilityInfo.setUpdatePeriod(duration);
        return retrievabilityInfo;
    }

    private Retrievability buildRetrievability(final RetrievabilityInfo... retrievabilityInfos) {
        final Retrievability retrievability = new Retrievability();
        retrievability.setBy(Arrays.asList(retrievabilityInfos));
        return retrievability;
    }
}
