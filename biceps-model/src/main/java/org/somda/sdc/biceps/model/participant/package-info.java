@DefaultQualifier(value = NotNull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.biceps.model.participant;

import org.jetbrains.annotations.NotNull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
