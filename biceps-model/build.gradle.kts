plugins {
    id("buildlogic.java-conventions")
    id("buildlogic.deploy")
    id("org.somda.sdc.xjc")
}


val jaxb: Configuration by configurations.creating

dependencies {
    api(projects.commonModel)
    api(projects.bicepsModelPlugins)
    api(libs.org.glassfish.jaxb.jaxb.core)
    api(libs.org.glassfish.jaxb.jaxb.runtime)
    api(libs.jakarta.xml.bind.jakarta.xml.bind.api)
    api(libs.io.github.threeten.jaxb.threeten.jaxb.core)
    api(libs.org.jvnet.jaxb.jaxb.plugins)
    api(libs.org.checkerframework.checker.qual)
    api(libs.org.jetbrains.annotations)

    testImplementation(libs.org.junit.jupiter.junit.jupiter.api)
    testImplementation(libs.org.junit.jupiter.junit.jupiter.engine)

    jaxb(libs.org.jetbrains.annotations)
    // plugins
    jaxb(projects.bicepsModelPlugins)
    jaxb(libs.org.jvnet.jaxb.jaxb.plugin.annotate)

}

description = "SDCri is a set of Java libraries that implements a network communication framework conforming " +
    "with the IEEE 11073 SDC specifications. This project implements the model for IEEE 11073-10207."

val schemaDir = "src/main/resources"

xjc {
    jaxbClasspath = jaxb
    schemaLocation = layout.projectDirectory.dir(schemaDir)
    args = listOf(
        "-XcustomExtensionTypeEquals",
        "-Xannotate",
    )
}

tasks.withType<Javadoc> {
    // generated code causes warnings, can't do anything about it :(
    (options as CoreJavadocOptions).addBooleanOption("Xdoclint:none", true)
}

tasks.withType<Checkstyle>().configureEach {
    // generated code
    enabled = false
}